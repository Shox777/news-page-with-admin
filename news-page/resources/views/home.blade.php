@extends('layouts.app')

@section('content')
<div class="container">
    <div class="flex">
        <div class="profile-side-nav">
            <h3>Categories</h3>
            <ul>
                @foreach ($categories as $category)
                <li> <a href="#">{{$category->name}}</a> </li>
                @endforeach
                
                
            </ul>
        </div>
        <div class="profile-content flex">
            @foreach ($products as $product)
            <a href=" {{route('home', ['category' => $category->slug])}} "><div><img src="img/pic.jpg" width="100%" alt=""> {{$product->name}} </div></a>
            @endforeach
        </div>
    </div>
</div>

@endsection
