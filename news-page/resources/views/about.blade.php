@extends('layouts.app')
@section('content-of-about')
<div class="container">
     <div class="flex">
         <div class="profile-side-nav">
             <ul>
                 <li> <a href="#">Side Links</a> </li>
                 <li> <a href="#">Side Links</a> </li>
                 <li> <a href="#">Side Links</a> </li>
                 <li> <a href="#">Side Links</a> </li>
                 <li> <a href="#">Side Links</a> </li>
                 <li> <a href="#">Side Links</a> </li>
                 <li> <a href="#">Side Links</a> </li>
             </ul>
         </div>
         <div class="about-content flex">
             <div><img src="img/pic.jpg" width="100%" alt="">Profile news content</div>
             <div>
                  <p>Lorem ipsum dolor sit amet consectetur 
                       adipisicing elit. Voluptates, praesentium 
                       sunt vitae ullam numquam sint tenetur 
                       sapiente? Ab, aut vero.Lorem ipsum dolor sit amet consectetur 
                       adipisicing elit. Voluptates, praesentium 
                       sunt vitae ullam numquam sint tenetur 
                       sapiente? Ab, aut vero.</p>
             </div>
         </div>
     </div>
 </div>  
@endsection
