<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="css/main.css">
       
    </head>
    <body>
            <div class="nav-links">
                @auth
                    <div>
                        <a href="#">Logo</a>
                    </div>
                    <div class="nav-link-space">
                        <a href="{{ url('/home') }}">Profile</a>
                        <a href="#">Fitness</a>
                        <a href="#">Travel</a>
                        <a href="#">Weather</a>
                        <a href="#">Cooking</a>
                    </div>
                    </div>
                    <div class="container">
                        <div class="flex">     
                            <div class="profile-content flex">
                                @foreach ($products as $product)
                                <a href="/about"><div><img src="img/pic.jpg" width="100%" alt=""> {{$product->name}} </div></a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @else
                    <div class="nav-link-space">
                        <a href="/about">World News</a>
                        <a href="#">Sport</a>
                        <a href="#">Music</a>
                        <a href="#">Travel</a>
                        <a href="#">COVID-19</a>
                    </div>
                    <div class="nav-link-auth">  
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    </div>                 
            </div> <!-- end of nav -->
            <div class="container">
                <div class="flex">     
                <div class="profile-content flex">
                    @foreach ($products as $product)
                    <a href="/about"><div><img src="img/pic.jpg" width="100%" alt=""> {{$product->name}} </div></a>
                    @endforeach
                    
                </div>
            </div>
            @endauth
    </body>
</html>
