<?php

use Illuminate\Database\Seeder;

use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        Product::create([
            'name' => ' Lorem ipsum dolor sit amet consectetur, adipisicing elit. A, ullam. ',
            'slug' => 'The Slug Name',
            'description' => ' Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quod, iure animi culpa nulla incidunt labore? ',  
        ])->categories()->attach(1);
        Product::create([
            'name' => ' The Title of News Content 1',
            'slug' => 'The Slug Name-1',
            'description' => ' Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quod, iure animi culpa nulla incidunt labore? ',  
        ])->categories()->attach(2);
        Product::create([
            'name' => ' The Title of News Content 2',
            'slug' => 'The Slug Name-2',
            'description' => ' Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quod, iure animi culpa nulla incidunt labore? ',  
        ])->categories()->attach(3);
        Product::create([
            'name' => ' The Title of News Content 3',
            'slug' => 'The Slug Name-3',
            'description' => ' Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quod, iure animi culpa nulla incidunt labore? ',  
        ])->categories()->attach(4);
        Product::create([
            'name' => ' The Title of News Content 4',
            'slug' => 'The Slug Name-4',
            'description' => ' Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quod, iure animi culpa nulla incidunt labore? ',  
        ])->categories()->attach(5);
    }
   /***  public function run()
    {
        //Covid-19 news
        for ($i = 1; i < 10; $i++) {
            Product::create([
                'name' => 'COVID-19 Lorem ipsum dolor sit amet consectetur, adipisicing elit. A, ullam.' .$i,
                'slug' => 'COVID-19 The Slug Name' .$i,
                'description' => ' Lorem' .$i. 'ipsum dolor, sit amet consectetur adipisicing elit. Quod, iure animi culpa nulla incidunt labore? ',  
            ])->categories()->attach(1);
        }
        //World news
        for ($i = 1; i < 10; $i++) {
            Product::create([
                'name' => 'WORLD NEWS-Lorem ipsum dolor sit amet consectetur, adipisicing elit. A, ullam.' .$i,
                'slug' => 'WORLD NEWS-The Slug Name' .$i,
                'description' => ' Lorem' .$i. 'ipsum dolor, sit amet consectetur adipisicing elit. Quod, iure animi culpa nulla incidunt labore? ',  
            ])->categories()->attach(2);
        }
        //Sports news
        for ($i = 1; i < 10; $i++) {
            Product::create([
                'name' => 'SPORT NEWS-Lorem ipsum dolor sit amet consectetur, adipisicing elit. A, ullam.' .$i,
                'slug' => 'SPORT NEWS-The Slug Name' .$i,
                'description' => ' Lorem' .$i. 'ipsum dolor, sit amet consectetur adipisicing elit. Quod, iure animi culpa nulla incidunt labore? ',  
            ])->categories()->attach(3);
        }
        //Fitness news
        for ($i = 1; i < 10; $i++){
            Product::create([
                'name' => 'FITNESS NEWS-Lorem ipsum dolor sit amet consectetur, adipisicing elit. A, ullam.' .$i,
                'slug' => 'FITNESS NEWS-The Slug Name' .$i,
                'description' => ' Lorem' .$i. 'ipsum dolor, sit amet consectetur adipisicing elit. Quod, iure animi culpa nulla incidunt labore? ',  
            ])->categories()->attach(4);
        }
        //Football news
        for ($i = 1; i < 10; $i++){
            Product::create([
                'name' => 'FOOTBALL NEWS-Lorem ipsum dolor sit amet consectetur, adipisicing elit. A, ullam.' .$i,
                'slug' => 'FOOTBALL NEWS-The Slug Name' .$i,
                'description' => ' Lorem' .$i. 'ipsum dolor, sit amet consectetur adipisicing elit. Quod, iure animi culpa nulla incidunt labore? ',  
            ])->categories()->attach(5);
        }
        //E-sport news
        for ($i = 1; i < 10; $i++){
            Product::create([
                'name' => 'E-SPORT NEWS-Lorem ipsum dolor sit amet consectetur, adipisicing elit. A, ullam.' .$i,
                'slug' => 'E-SPORT NEWS-The Slug Name' .$i,
                'description' => ' Lorem' .$i. 'ipsum dolor, sit amet consectetur adipisicing elit. Quod, iure animi culpa nulla incidunt labore? ',  
            ])->categories()->attach(6);
        }
    } */
}
