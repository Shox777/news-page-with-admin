<?php
use App\Category;
use Carbon\Carbon;

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();

        Category::insert([
            ['name' => 'COVID-19','slug' => 'The Slug Name', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Global News','slug' => 'world-1', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Sports','slug' => 'sport-n1', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Fitness Advice','slug' => 'fitness', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Football','slug' => 'football-1', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'E-sports','slug' => 'esport', 'created_at' => $now, 'updated_at' => $now],
        ]);
    }
}
