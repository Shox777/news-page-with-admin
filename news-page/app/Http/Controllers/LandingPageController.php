<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;

class LandingPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::take(100)->get();
        return view('landing-page')->with('products', $product);
    }
}
