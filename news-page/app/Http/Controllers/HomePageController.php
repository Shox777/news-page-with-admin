<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use App\Category;

class HomePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->category){
            $products = Product::with('categories')->whereHas('categories', function($query){
                $query->where('slug', request()->category);
            })->get();
            $categories = Category::all();
        }
        else{ 

        $product = Product::take(100)->get();
        $categories = Category::all();
    }
        return view('home')->with([
            'products'=> $product,
            'categories' => $categories,
        ]);
    }

    
}
